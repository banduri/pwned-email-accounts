# german sentiment
# von: http://wortschatz.uni-leipzig.de/de/download#sentiWSDownload
# Der SentimentWortschatz, oder kurz SentiWS, ist eine öffentlich verfügbare deutschsprachige Ressource für die Sentiment Analyse, Opinion Mining und ähnliche Zwecke. Dabei werden für enthaltene Wörter die positive und negative Polarität im Intervall [-1; 1] angegeben, sowie deren Wortart und (falls anwendbar) Flexionsvarianten. Die aktuelle Version des SentiWS enthält ungefähr 1.650 positive und 1.800 negative Grundformen, so dass, inklusive der verschiedenen Flexionsformen, insgesamt etwa 16.000 positive und 18.000 negative Wortformen enthalten sind. SentiWS enthält nicht nur Adjektive und Adverbien, sondern auch Nomen und Verben die Träger von Sentiment sind.
# SentiWS liegt in zwei UTF8-kodierten Textdateien vor und ist folgendermaßen strukturiert: | \t \t ,..., \n mit \t als Tabulator und \n als Zeilenumbruch. SentiWS steht unter der Creative Commons Attribution-Noncommercial-Share Alike 3.0 Unported Lizenz. Falls Sie SentiWS für Ihre Arbeit einsetzen, bitten wir Sie diese Veröffentlichung in der folgenden Form zu zitieren:
# R. Remus, U. Quasthoff & G. Heyer: SentiWS - a Publicly Available German-language Resource for Sentiment Analysis. In: Proceedings of the 7th International Language Ressources and Evaluation (LREC'10), pp. 1168-1171, 2010 Download SentiWS:
# v2.0, 2018-10-19: Dritte öffentliche Version (Überarbeitung der flektierten Formen)
# Ein kurzes durchzaehlen ergibt:
# 1504 ADJX (Adjektiv) 11 ADV (Adverben) 1236 NN (Nomen) 720 VVINF (Verben)

# Die nachfolgende SentimentMap ist ein dict 
# wobei der lookup index 'word' caseinsensitiv ist
# das Attribute 'word' enhaelt orginalwort mit CamelCase (so vorhanden)
# 'tag' ist in [ADJX,ADV,NN,VVINF]
# 'score' ist der polaritaetswert
# 'parent' ist das Stammwort, des Wortes

# Berechnungen / Normalisierung
# die Annahme ist, dass das scoring der Sentimentmap nicht gleich verteilt ist, 
# es gibt mehr negative als positive Wertungen. Die meisten werte Sollten sich um den Nullpunkt bewegen -> Gaussverteilung
# 'nscore' ist der polaritaetswert um den Durchschnitt zentriert und mit skalierung der positiv wie negativ X-Achse auf 1
#
#SentimentMap[word]={
#   'word': w,
#   'tag': tag,
#   'score': float(score),
#   'parent': oword
# }



import csv
from pandas import DataFrame
from collections import defaultdict

SentimentMap = {}
sdata = defaultdict(list)
for filename in [
    "data/SentiWS_v2.0_Negative.txt",
    "data/SentiWS_v2.0_Positive.txt"
]:

    with open(filename) as fp:
        cvsr = csv.reader(fp,delimiter="\t")
        for row in cvsr:

            oword,tag = row[0].split("|")
            score = row[1]
            words = row[2].split(",")
            words.append(oword)
            for w in words:
                if len(w) > 0:
                    # case insensitv
                    word = w.lower()


                    SentimentMap[word]={
                        'word': w,
                        'tag': tag,
                        'score': float(score),
                        'parent': oword
                    }
                    sdata['word'].append(word)
                    sdata['tag'].append(tag)
                    sdata['score'].append(float(score))

sdf=DataFrame(sdata['score'])
smin = float(sdf.min())
smax = float(sdf.max())
smean = float(sdf.mean())

for word in SentimentMap:
    SentimentMap[word]['nscore'] = float((SentimentMap[word]['score'] - smean))
    if SentimentMap[word]['nscore'] < 0.0:
        SentimentMap[word]['nscore'] = SentimentMap[word]['nscore'] / abs(smin - smean)
    else:
        SentimentMap[word]['nscore'] = SentimentMap[word]['nscore'] / abs(smax - smean)
    
    
