%matplotlib inline

import matplotlib.pyplot as plt
from pandas import DataFrame,DatetimeIndex

from datetime import datetime


# Problembeschreibung:
# Wie aendert sich die Aeufigkeit eines events in der Tagesstundenansicht.
#
# Fragestellung:
# Gibt es Schwankungen zwischen unterschiedlichen Betrachtungszeitraeumen?

# Betrachtungszeitraum
betrstart=datetime(year=2019,month=1,day=28)
betrend=datetime(year=2019,month=3,day=17) 

# es wird in 'count' int('1') erwartet. In diesem Fall hier werden Artikel
# gezaehlt.
# Es wird ein Filter angewendet um auf die Autorenanzahl zu filtern
# der betrachtungszeitraum wird mit dem datetimeobjekt in 'pubdate' abgeglichen
odf = DataFrame(data)

for minauthorcount in [[0],[0,1,2,3,4,5,6],[1,2,3,4,5,6],[1],[2],[3]]:
    print("Artikel mit %s Autoren" %(",".join(map(lambda x: str(x),minauthorcount))))
    df = odf.copy()
    # Count nach Artikel umbenennen
    df = df.rename(index=str, columns={"count": "Artikel"})

    # auf Autorenanzahl und datum filtern
    df = df[df['Autorenanzahl'].isin(minauthorcount)]
    ddf = df[df['pubdate'] <= betrend]
    ddf = ddf[ddf['pubdate'] >= betrstart]

    dfdate=ddf
    #display(ddf.describe())
    stunde = DatetimeIndex(dfdate['pubdate'])
    stunde.rename('Stunde',inplace=True)
    stunde = stunde.hour

    woche = DatetimeIndex(dfdate['pubdate'])
    woche.rename('Woche',inplace=True)
    woche = woche.week


    tag = DatetimeIndex(dfdate['pubdate'])
    tag.rename("Tag",inplace=True)

    #display(tag)
    wochentag = DatetimeIndex(dfdate['pubdate'])
    wochentag.rename('Wochentag',inplace=True)
    #DatetimeIndex.dayofweek
    #    The day of the week with Monday=0, Sunday=6
    wochentag = wochentag.dayofweek

    monat = DatetimeIndex(dfdate['pubdate'])
    monat.rename('Monat',inplace=True)
    monat = monat.month

    dfdate = dfdate.set_index(['pubdate'])
    dfdate = dfdate.groupby([wochentag,stunde]).agg({'Artikel':'sum'})
    dfu=dfdate


    dfp=dfu.reset_index().pivot("Wochentag","Stunde")
    display(dfp.head(10))



    ax = dfp.plot(grid=True,kind='box',showmeans=True,

                     title='%d zeit-Artikel \n von %s bis %s (%d Tage)' %(len(ddf['Artikel']),
                                                                         betrstart.date(),
                                                                         betrend.date(),
                                                                         (betrend - betrstart).days))
    ax.set_ylim(top=30)
    #ax.set_xlim(left=2, right=18)
    plt.xticks(rotation=90)


    plt.show()
